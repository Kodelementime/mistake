package com.kodelementime.textures;

import java.io.Serializable;


public class ModelTexture implements Serializable {

	private int textureID;
	private int width;
	private int height;
	private Texture tex;
	
	private float shineDumper = 1;
	private float reflectivity = 0;
	
	private boolean hasTranprency = false;
	private boolean useFakeLighning = false;
	
	public ModelTexture(Texture tex) {
		this.tex = tex;
		this.textureID = tex.id;
		this.width = tex.width;
		
	}
	

	public boolean isUseFakeLighning() {
		return useFakeLighning;
	}


	public void setUseFakeLighning(boolean useFakeLighning) {
		this.useFakeLighning = useFakeLighning;
	}




	public boolean hasTranprency() {
		return hasTranprency;
	}


	public void setHasTranprency(boolean hasTranprency) {
		this.hasTranprency = hasTranprency;
	}


	public void setTextureID(int textureID) {
		this.textureID = textureID;
	}

	public int getTextureID() {
		return textureID;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public Texture getTex() {
		return tex;
	}


	public float getShineDumper() {
		return shineDumper;
	}


	public void setShineDumper(float shineDumper) {
		this.shineDumper = shineDumper;
	}


	public float getReflectivity() {
		return reflectivity;
	}


	public void setReflectivity(float reflectivity) {
		this.reflectivity = reflectivity;
	}

	
	
	
	
	

}
