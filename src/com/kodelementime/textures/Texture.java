package com.kodelementime.textures;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.ByteBuffer;

import javax.imageio.ImageIO;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;

public class Texture implements Serializable {
	
	public int id, width, height;
	private BufferedImage bi;
	
	public Texture(String file) {
		
		try {
			bi = ImageIO.read(new File("res/" +file +".png"));
			height = bi.getHeight();
			width = bi.getWidth();
		
		} catch (IOException e) {
			e.printStackTrace();
		
		}
		
	}
	
	
	public void loadTexture(){
		int[] pixels_raw = new int[width * height * 4];
		pixels_raw = bi.getRGB(0, 0, width, height, null, 0 ,width);
		
		ByteBuffer pixels = BufferUtils.createByteBuffer(width*height*4);
		
		for(int i = 0; i < height; i++){
			for(int j = 0; j < width; j++){
				int pixel = pixels_raw[i*width + j];
				pixels.put((byte)((pixel >> 16) & 0xFF)); //RED
				pixels.put((byte)((pixel >> 8) & 0xFF)); //GREEN
				pixels.put((byte)(pixel & 0xFF)); //BLUE
				pixels.put((byte)((pixel >> 24) & 0xFF)); //ALPHA
			}
		}
		
		pixels.flip();
		
		id = GL11.glGenTextures();
		
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, id);
		
		GL11.glTexParameterf(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST);
		GL11.glTexParameterf(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
		
		GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL11.GL_RGBA, width, height, 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, pixels);
	}

	public int getId() {
		return id;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}
	
	

}
