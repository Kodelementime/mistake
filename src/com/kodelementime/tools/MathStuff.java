package com.kodelementime.tools;

import org.joml.Matrix4f;
import org.joml.Vector3f;

import com.kodelementime.entities.Camera;

public class MathStuff {

	
	public static Matrix4f createTransformationMatrix(Vector3f translation, float rx, float ry, float rz, float s){
		Matrix4f matrix = new Matrix4f();
		matrix.identity().translate(translation).
         rotateX((float)Math.toRadians(rx)).
         rotateY((float)Math.toRadians(ry)).
         rotateZ((float)Math.toRadians(rz)).
         scale(s);
		return matrix;
	}
	
	
	public static Matrix4f createViewMatrix(Camera camera) {
		Matrix4f viewMatrix = new Matrix4f();
		Vector3f cameraPos = camera.getPosition();
		cameraPos = new Vector3f(-cameraPos.x, -cameraPos.y,-cameraPos.z);

		viewMatrix.identity().translate(cameraPos);
		if(!camera.isOrtographic()) viewMatrix.rotateX((float)Math.toRadians(camera.getPitch()));
		else viewMatrix.rotateX((float)Math.toRadians(-camera.getPitch()));
		viewMatrix.rotateY((float)Math.toRadians(camera.getYaw()));
		if(!camera.isOrtographic()) viewMatrix.rotateZ((float)Math.toRadians(camera.getRoll()));
		else viewMatrix.rotateZ((float)Math.toRadians(camera.getRoll() + 180));
		
		
		return viewMatrix;
	}
	
	
	

}
