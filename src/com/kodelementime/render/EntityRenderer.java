package com.kodelementime.render;

import java.util.List;
import java.util.Map;

import org.joml.Matrix4f;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

import com.kodelementime.entities.Entity;
import com.kodelementime.models.RawModel;
import com.kodelementime.shaders.StaticShader;
import com.kodelementime.textures.ModelTexture;
import com.kodelementime.tools.MathStuff;

public class EntityRenderer {
	

	
	private StaticShader shader;
	
	public EntityRenderer(StaticShader shader, Matrix4f pM, Matrix4f oPM){
		this.shader = shader;
		shader.start();
		shader.loadProjectionMatrix(pM);
		shader.loadOrthoProjectionMatrix(oPM);
		shader.stop();
		
	}
	
	
	public void render(Map<TMInstance, List<Entity>> entities){
		for(TMInstance mT:entities.keySet()){
			preapareTexturedModel(mT);
			List<Entity> batch = entities.get(mT);
			for(Entity en:batch){
				preapareInstance(en);
				GL11.glDrawElements(GL11.GL_TRIANGLES, mT.getModel().getVertexCount(), GL11.GL_UNSIGNED_INT, 0);
			}
			unBindTexturedModel();
		}
	}
	
	private void preapareTexturedModel(TMInstance mT){
		RawModel r =  mT.getModel();
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL30.glBindVertexArray(r.getVaoID());
		GL20.glEnableVertexAttribArray(0);
		GL20.glEnableVertexAttribArray(1);
		GL20.glEnableVertexAttribArray(2);
		ModelTexture tex = mT.getTexture();
		if(tex.hasTranprency())
			MasterRenderer.disableCulling();
		shader.loadShineValues(tex.getShineDumper(), tex.getReflectivity(), tex.isUseFakeLighning());
		GL13.glActiveTexture(GL13.GL_TEXTURE0);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, tex.getTextureID());
	}
	
	private void unBindTexturedModel(){
		MasterRenderer.enableCulling();
		GL20.glDisableVertexAttribArray(2);
		GL20.glDisableVertexAttribArray(1);
		GL20.glDisableVertexAttribArray(0);
		GL30.glBindVertexArray(0);
		GL11.glDisable(GL11.GL_TEXTURE_2D);
	}
	
	
	private void preapareInstance(Entity entity){
		Matrix4f transformationMatrix = MathStuff.createTransformationMatrix(entity.getPosition(), 
				entity.getRotX(), entity.getRotY(), entity.getRotZ(), entity.getScale());
		shader.loadTransformationMatrix(transformationMatrix);
	}
	
	
	
	
	


}
