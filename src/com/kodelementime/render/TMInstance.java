package com.kodelementime.render;

import com.kodelementime.models.RawModel;
import com.kodelementime.textures.ModelTexture;

public class TMInstance {

	private ModelTexture texture;
	private RawModel model;

	public ModelTexture getTexture() {
		return texture;
	}

	public RawModel getModel() {
		return model;
	}

	public void setTexture(ModelTexture texture) {
		this.texture = texture;
	}

	public void setModel(RawModel model) {
		this.model = model;
	}
	
	
	
	
	

}
