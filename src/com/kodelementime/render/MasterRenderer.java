package com.kodelementime.render;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.lwjgl.opengl.GL11;

import com.kodelementime.entities.Camera;
import com.kodelementime.entities.Entity;
import com.kodelementime.entities.Light;
import com.kodelementime.shaders.StaticShader;
import com.kodelementime.shaders.TerrainShader;
import com.kodelementime.terrains.Terrain;

public class MasterRenderer {

	private StaticShader shader = new StaticShader(); 
	private EntityRenderer renderer;
	
	private TerrainShader terrainShader = new TerrainShader();
	private TerrainRenderer tRenderer;
	
	private static final float FOV = 70;
	private static final float NEAR_PLANE = 0.1f;
	private static final float FAR_PLANE = 1000f;
	
	private static final float RED = 0f;
	private static final float GREEN = 0f;
	private static final float BLUE = 0;
	
	private Matrix4f projectionMatrix, orthoProjectionMatrix;
	
	
	private Map<TMInstance, List<Entity>> entities =  new HashMap<TMInstance,List<Entity>>();
	private List<Terrain> terrains = new ArrayList<Terrain>();
	
	public MasterRenderer(){
		enableCulling();
		orthoProjectionMatrix = createOrthoProjectionMatrix( 640, 480);
		projectionMatrix = createProjectionMatrix(640, 480);
		renderer = new EntityRenderer(shader, projectionMatrix, orthoProjectionMatrix);
		tRenderer = new TerrainRenderer(terrainShader, projectionMatrix, orthoProjectionMatrix);
		
	}
	
	
	public static void enableCulling(){
		GL11.glEnable(GL11.GL_CULL_FACE);
		GL11.glCullFace(GL11.GL_BACK);
	}
	
	
	public static void disableCulling(){
		GL11.glDisable(GL11.GL_CULL_FACE);
	}
	
	
	
	public void preapare(List<Light> lights, Camera cam){
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
		GL11.glClearColor(RED, GREEN, BLUE, 1);
		shader.start();
		shader.loadSkyColor(new Vector3f(RED,GREEN,BLUE));
		for(Light i:lights)
			shader.loadLight(i);
		shader.loadViewMatrix(cam);
		shader.stop();
		terrainShader.start();
		terrainShader.loadSkyColor(new Vector3f(RED,GREEN,BLUE));
		for(Light i:lights)
			terrainShader.loadLight(i);
		terrainShader.loadViewMatrix(cam);
		terrainShader.stop();
		
	}
	
	public void processEntity(Entity entity){
		TMInstance tM = entity.getInstance();
		List<Entity> batch = entities.get(tM);
		if(batch != null){
			batch.add(entity);
		}else{
			List<Entity> newBatch = new ArrayList<Entity>();
			newBatch.add(entity);
			entities.put(tM, newBatch);
		}
	}
	
	public void processTerrain(Terrain ter){
		terrains.add(ter);
	}
	
	
	public void render(){
		shader.start();
		renderer.render(entities);
		shader.stop();
		terrainShader.start();
		tRenderer.render(terrains);
		terrainShader.stop();
	}
	
	
	public void end(){
		terrains.clear();
		entities.clear();
	}
	
	public void cleanUp(){
		terrainShader.cleanUP();
		shader.cleanUP();
	}
	
public Matrix4f createProjectionMatrix(float width, float height){
		
		
        float aspectRatio =  width /  height;
        Matrix4f projectionMatrix = new Matrix4f();
        projectionMatrix.identity();
        projectionMatrix.perspective(FOV, aspectRatio,
        		NEAR_PLANE, FAR_PLANE); 
        
		return projectionMatrix;

        
        
	}
	
	
	public Matrix4f createOrthoProjectionMatrix(float width, float height){
		
		float aspectRatio =  width /  height;
        Matrix4f projectionMatrix = new Matrix4f();
        projectionMatrix.identity();
        projectionMatrix.setOrtho(-aspectRatio, aspectRatio, 1, -1, 0, FAR_PLANE).scale(0.5f);
        
		
		return projectionMatrix;
        
        
	}
	

}
