package com.kodelementime.render;

import java.util.List;

import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

import com.kodelementime.entities.Entity;
import com.kodelementime.models.RawModel;
import com.kodelementime.shaders.TerrainShader;
import com.kodelementime.terrains.Terrain;
import com.kodelementime.textures.ModelTexture;
import com.kodelementime.tools.MathStuff;

public class TerrainRenderer {

	private TerrainShader shader;
	
	public TerrainRenderer(TerrainShader terrainShader, Matrix4f pM, Matrix4f oPM) {
		this.shader = terrainShader;
		shader.start();
		shader.loadProjectionMatrix(pM);
		shader.loadOrthoProjectionMatrix(oPM);
		shader.stop();
	}
	
	public void render(List<Terrain> terrains){
		for(Terrain ter:terrains){
			preapareTerrain(ter);
			loadModelMatrix(ter);
			GL11.glDrawElements(GL11.GL_TRIANGLES, ter.getModel().getVertexCount(), GL11.GL_UNSIGNED_INT, 0);
			unBindTexturedModel();
		}
		
	}
	
	private void preapareTerrain(Terrain ter){
		RawModel r =  ter.getModel();
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL30.glBindVertexArray(r.getVaoID());
		GL20.glEnableVertexAttribArray(0);
		GL20.glEnableVertexAttribArray(1);
		GL20.glEnableVertexAttribArray(2);
		ModelTexture tex = ter.getTex();
		shader.loadShineValues(tex.getShineDumper(), tex.getReflectivity());
		GL13.glActiveTexture(GL13.GL_TEXTURE0);
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, tex.getTextureID());
	}
	
	private void unBindTexturedModel(){
		GL20.glDisableVertexAttribArray(2);
		GL20.glDisableVertexAttribArray(1);
		GL20.glDisableVertexAttribArray(0);
		GL30.glBindVertexArray(0);
		GL11.glDisable(GL11.GL_TEXTURE_2D);
	}
	
	
	private void loadModelMatrix(Terrain ter){
		Matrix4f transformationMatrix = MathStuff.createTransformationMatrix(new Vector3f(ter.getX(), 0, ter.getZ()), 
				0, 0, 0, 1);
		shader.loadTransformationMatrix(transformationMatrix);
	}

}
