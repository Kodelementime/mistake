package com.kodelementime.core;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.naming.spi.ObjectFactory;
import javax.naming.spi.ObjectFactoryBuilder;

import org.joml.Vector3f;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.omg.CORBA.ObjectHelper;

import com.kodelementime.core.displays.Display;
import com.kodelementime.core.displays.MainDisplay;
import com.kodelementime.core.displays.SideDisplay;
import com.kodelementime.entities.Camera;
import com.kodelementime.entities.Entity;
import com.kodelementime.entities.Light;
import com.kodelementime.terrains.Terrain;


public class Core{
	

	public static int mW, mH;
	
	public static void main(String[] args) {
		Core core = new Core();
		core.Init();
	}
	
	
	public void Init()
	{
		GLFWErrorCallback.createPrint(System.err).set();
		boolean isInitialized  = GLFW.glfwInit();
		if(!isInitialized){
			System.err.println("Failed To initialized!");
			System.exit(1);
		}
		
		GLFWVidMode mode = GLFW.glfwGetVideoMode(GLFW.glfwGetPrimaryMonitor());
		
		mW = mode.width();
		mH = mode.height();
		
		Scene scene = new Scene("Mistake Main");
		
		
		
		
		scene.addLight(new Light(new Vector3f(-1000,1000,1000), new Vector3f(1,1,1)));
		
		scene.addModel("Cactus/cactus");
		scene.addTexture("Cactus/cactus", 1, 10, false, false);
		scene.addTexture("ter", 1, 10, false, false);
		Random rand = new Random();
		
		scene.addEntity(new Entity(0, 0, new Vector3f(0,0,-10), 0, 0, 0, 1));
		
		Scene scene2 = new Scene("Side", scene);
		Scene scene3 = new Scene("Finder", scene);
		
		Camera ortho = new Camera(new Vector3f(0,0,10), true);
		
		//ortho.setRoll(180);
		Camera pers = new Camera(new Vector3f(0,-3,10), false);
		pers.setPitch(30);
		pers.setYaw(45);
		
		
		
		
		scene.setCamera(ortho);
		scene2.setCamera(new Camera(ortho));
		scene3.setCamera(new Camera(ortho));
		
		Display main = new  MainDisplay((mW - 640) / 2, (mH -480) / 2, "My Suicide Note", 640, 480, scene);
		//main.setDecoration(false);
		new  SideDisplay(0, main.getWidth(), main.getHeight(), "Red", 10, 100, main, scene2).setDecoration(false);
		new  SideDisplay((float)Math.PI/2f, main.getWidth(), main.getHeight(), "Red", 10, 100, main, scene2).setDecoration(false);
		new  SideDisplay((float)Math.PI, main.getWidth(), main.getHeight(), "Red", 10, 100, main, scene2).setDecoration(false);
		new  SideDisplay(3f*(float)Math.PI/2f, main.getWidth(), main.getHeight(), "Red", 10, 100, main, scene2).setDecoration(false);
		new  SideDisplay(0, main.getWidth(), 0, "Red", 10, 100, main, scene2).setDecoration(false);
	
		main.getFocus();
		
		while(!main.shouldClose()){
			
			
			for(Display dis:Display.displays)
			{
				dis.Update();
				dis.Render();
			}
			
			LUpdate();
			
			GLFW.glfwPollEvents();
			
		}
		for(Display dis:Display.displays)
			dis.cleanUp();
		Loader.CleanUp();
		GLFW.glfwTerminate();
		System.exit(1);
	}
	
	
	public void LUpdate(){

		
	}
	
	
	public static boolean getKey(int key){
		boolean is = false;
		for(Display d:Display.displays){
			int tmp =  GLFW.glfwGetKey(d.getWindow(), key);
			is = tmp == GLFW.GLFW_TRUE;
			if(is) break;
		}
		return is;
	}
	

	public static boolean getKey(int key, long window){
		int tmp =  GLFW.glfwGetKey(Display.displays.get(0).getWindow(), key);
		return tmp == GLFW.GLFW_TRUE;
	}

}
