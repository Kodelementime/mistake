package com.kodelementime.core.displays;

import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import org.joml.Vector3f;
import org.lwjgl.BufferUtils;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11;
import org.lwjgl.system.MemoryUtil;

import com.kodelementime.core.Core;
import com.kodelementime.core.Scene;
import com.kodelementime.entities.Camera;
import com.kodelementime.entities.Entity;
import com.kodelementime.entities.Light;
import com.kodelementime.render.MasterRenderer;
import com.kodelementime.shaders.StaticShader;

public class Display implements Runnable {

	public static List<Display> displays = new ArrayList<Display>();

	protected Scene scene;
	protected long window;
	protected int width, height , x, y;
	private IntBuffer xA, yA;
	protected MasterRenderer renderer;
	private long lastNanos;
	public Display(int x, int y, String title, int width, int height, long share, Scene scene){
		
		GLFWErrorCallback.createPrint(System.err).set();
		boolean isInitialized  = GLFW.glfwInit();
		if(!isInitialized){
			System.err.println("Failed To initialized!");
			System.exit(1);
		}
		
		GLFWVidMode mode = GLFW.glfwGetVideoMode(GLFW.glfwGetPrimaryMonitor());
		
		Core.mW = mode.width();
		Core.mH = mode.height();	

		
		window = GLFW.glfwCreateWindow(width, height, title, MemoryUtil.NULL, share);
		GLFW.glfwSetWindowPos(window, x,y);
		this.x = x; this.y = y;
		this.width = width;
		this.height = height;
		
		
	
		
		
		GLFW.glfwSetWindowAttrib(window, GLFW.GLFW_RESIZABLE, GLFW.GLFW_FALSE);
			GLFW.glfwMakeContextCurrent(window);
		GL.createCapabilities();
		renderer = new MasterRenderer();
		
		
		
		
		this.scene = scene;
		displays.add(this);
		init();
		GL11.glViewport(0, 0, 640, 480);
		
		xA = BufferUtils.createIntBuffer(1);
		yA = BufferUtils.createIntBuffer(1);
		
		
	}
	
	
	
	
	public Camera getCamera() {
		return getScene().getCamera();
	}




	public void setCamera(Camera camera) {
		getScene().setCamera(camera);
		System.out.println(getScene().getCamera().getSpeed());
	}




	public void init(){
		lastNanos = System.nanoTime();
		scene.Init();
	}
	
	


	public Scene getScene() {
		return scene;
	}


	public void setScene(Scene scene) {
		this.scene = scene;
	}


	public Display(int x, int y, String title, int width, int height, Scene scene){
		this( x,  y,  title,  width,  height,MemoryUtil.NULL, scene);
	}
	
	
	public boolean isFocused(){
		return GLFW.glfwGetWindowAttrib(window, GLFW.GLFW_FOCUSED) == GLFW.GLFW_TRUE;
	}
	
	
	public void setResizability(boolean resizable){
		int i = resizable ? GLFW.GLFW_TRUE : GLFW.GLFW_FALSE;
		GLFW.glfwSetWindowAttrib(window, GLFW.GLFW_RESIZABLE, i);
	}
	
	
	public void Destroy()
	{
		List<Display> tmp = displays;
		displays = new ArrayList<Display>();
		for(Display d:tmp)
			if(d != this)
				displays.add(d);
		
		GLFW.glfwSetWindowShouldClose(window, true);
		Hide();
	}
	

	public void setDecoration(boolean decorated){
		int i = decorated ? GLFW.GLFW_TRUE : GLFW.GLFW_FALSE;
		GLFW.glfwSetWindowAttrib(window, GLFW.GLFW_DECORATED, i);
	}
	
	
	public void Update(){
		 syncFrameRate(60, lastNanos);
		 lastNanos = System.nanoTime();
		
		if(getScene().getCamera() != null){
			getScene().getCamera().Move();
		}
		getScene().Update();
		GLFW.glfwGetWindowPos(window, xA, yA);
		
		this.x = xA.get(0);
		this.y = yA.get(0);
	}
	
	
	public void getFocus(){
		GLFW.glfwFocusWindow(window);
	}
	
	public void Show(){
		GLFW.glfwShowWindow(window);
	}
	
	public void Hide(){
		GLFW.glfwHideWindow(window);
	}
	
	
	public void Render(){
		preapare();
		loop();
		end();
	}
	
	public void cleanUp()
	{
		renderer.cleanUp();
	}
	
	
	public boolean shouldClose(){
		return GLFW.glfwWindowShouldClose(window);
	}
	
	
	public int isIconified(){
		return GLFW.glfwGetWindowAttrib(window, GLFW.GLFW_ICONIFIED);
	}
	
	

	
	
	protected void preapare(){
		GLFW.glfwMakeContextCurrent(window);
		renderer.preapare(scene.getLights(), scene.getCamera());
		
	}
	
	
	public void loop(){
		scene.Render(renderer);
		renderer.render();
	}
	
	protected void end(){
		renderer.end();
		GLFW.glfwSwapBuffers(window);
	}
	

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public void setPos(int x, int y) {
		this.x = x;
		this.y = y;
		GLFW.glfwSetWindowPos(window, x, y);
	}
	
	
	public void Move(int x, int y){
		this.x += x;
		this.y += y;
		GLFW.glfwSetWindowPos(window, this.x, this.y);
	}
	

	public int getWidth() {
		return width;
	}

	public void setSize(int width, int height) {
		GLFW.glfwSetWindowSize(window, width, height);
		this.width = width;
		this.height = height;
	}

	public int getHeight() {
		return height;
	}

	

	public long getWindow() {
		return window;
	}
	
	
	
	public MasterRenderer getRenderer() {
		return renderer;
	}




	public void setRenderer(MasterRenderer renderer) {
		this.renderer = renderer;
	}




	private void syncFrameRate(float fps, long lastNanos) {
        long targetNanos = lastNanos + (long) (1_000_000_000.0f / fps) - 1_000_000L;  // subtract 1 ms to skip the last sleep call
        try {
            while (System.nanoTime() < targetNanos) {
                Thread.sleep(1);
            }
        }
        catch (InterruptedException ignore) {}
    }




	@Override
	public void run() {
		while(!shouldClose()){
			Update();
			Render();
			GLFW.glfwPollEvents();
		}
		cleanUp();
		GLFW.glfwTerminate();
	}
		
}
	
	
	

