package com.kodelementime.core.displays;


import org.joml.Math;
import org.joml.Vector3f;
import org.lwjgl.glfw.GLFW;

import com.kodelementime.core.Core;

import com.kodelementime.core.Scene;


public class SideDisplay extends Display {
	
	private Display dis;
	private int offsetX, offsetY;
	private boolean free = false;
	public SideDisplay(float theta, int offsetX, int offsetY, String title, int width, int height, Display d, Scene scene) {
		super(d.getX() + d.getWidth() + offsetX, d.getY() + offsetY, title, width, height, scene);
		//setDecoration(false);
		setRenderer(d.getRenderer());
		this.offsetX = offsetX;
		this.offsetY = offsetY;
		dis = d;
		
		this.theta = theta;
		
		
	}
	
	boolean press, prv_press;
	
	@Override
	public void init() {
		super.init();
	}
	
	
	public boolean fixed = false;
	float theta;
	@Override
	public void Update() {
		
		super.Update();
		press = Core.getKey(GLFW.GLFW_KEY_E);
	
		if(!press && prv_press){
			free = !free;
			System.out.println(free);
		}
		
		
		if(!free){
			
			theta += 0.01f;
			float x =  (float)dis.getX() + (float)dis.width/2f - (float)this.height+ offsetX* (float)Math.cos(theta);
			float y =  (float)dis.getY() + (float)dis.height/2f -  (float)this.height+offsetY * (float)Math.sin(theta);
			
			setPos((int)x, (int)y);
			if(isFocused())
				dis.getFocus();
			fixed = false;
			if(isIconified() != dis.isIconified())
				GLFW.glfwSetWindowAttrib(window, GLFW.GLFW_ICONIFIED, dis.isIconified());
			
			if(!fixed){ 
				float distanceX = (float)this.getX() - (float)dis.getX();
				float distanceY = ((float)this.getY() + (float)this.getHeight()) - ((float)dis.getY() + (float)dis.getHeight());
				distanceX = (((distanceX / 320f) * (640f/480f)) / dis.getCamera().getCloseScale()) + dis.getCamera().getPosition().x;
				distanceY = ((distanceY / 240f) / dis.getCamera().getCloseScale()) + dis.getCamera().getPosition().y ;
				getCamera().setPosition(new Vector3f( (distanceX), + distanceY, dis.getCamera().getPosition().z));
				
			
				
				fixed = true;
			}

		}else{
			
			
			
			float distanceX = (float)this.getX() - (float)dis.getX();
			float distanceY = ((float)this.getY() + (float)this.getHeight()) - ((float)dis.getY() + (float)dis.getHeight());
			distanceX = (((distanceX / 320f) * (640f/480f)) / dis.getCamera().getCloseScale()) + dis.getCamera().getPosition().x;
			distanceY = ((distanceY / 240f) / dis.getCamera().getCloseScale()) + dis.getCamera().getPosition().y ;
			getCamera().setPosition(new Vector3f( (distanceX), distanceY, dis.getCamera().getPosition().z));
			if(fixed) fixed = false;
			
		}
		
		prv_press = press;
		
	}
	
	
	@Override
	public void loop() {
		super.loop();
	}


}
