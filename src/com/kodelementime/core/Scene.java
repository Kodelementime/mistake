package com.kodelementime.core;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.opengl.GL11;

import com.kodelementime.entities.Camera;
import com.kodelementime.entities.Entity;
import com.kodelementime.entities.Light;
import com.kodelementime.models.ModelData;
import com.kodelementime.models.OBJLoader;
import com.kodelementime.models.RawModel;
import com.kodelementime.render.MasterRenderer;
import com.kodelementime.shaders.StaticShader;
import com.kodelementime.terrains.Terrain;
import com.kodelementime.textures.ModelTexture;
import com.kodelementime.textures.Texture;

public class Scene implements Serializable {

	public transient static List<Entity> sharedEntities = new ArrayList<Entity>();
	
	public List<Entity> entities;
	public List<Terrain> terrains;
	
	
	private String name;
	private Camera camera;
	
	
	
	private List<float[]> vertecies;
	private List<float[]> texCoords;
	private List<float[]> normals;
	private List<int[]> indicies;
	private List<Light> lights;
	
	
	
	
	private transient List<Float> shineDumpers;
	private transient List<Float> reflectivities;
	private transient List<Boolean> hasTransparencies;
	private transient List<Boolean> useFakeLightnings;
	
	private List<ModelTexture> mTexs;
	
	private transient List<String> textureLocations = new ArrayList<String>();
	private transient List<Texture> textures  = new ArrayList<Texture>();
	private transient List<RawModel> rawModels  = new ArrayList<RawModel>();
	
	
	public Scene(String name){
		this.name = name;
		entities =  new ArrayList<Entity>(); 
		terrains = new ArrayList<Terrain>();
		
		vertecies = new ArrayList<float[]>();
		texCoords = new ArrayList<float[]>();
		normals = new ArrayList<float[]>();
		indicies = new ArrayList<int[]>();
		lights = new ArrayList<Light>();
		
		
		shineDumpers = new ArrayList<Float>();
		reflectivities = new ArrayList<Float>();
		hasTransparencies = new ArrayList<Boolean>();
		useFakeLightnings = new ArrayList<Boolean>();
		mTexs = new ArrayList<ModelTexture>();
		
	}
	
	
	
	public Scene(String name, Scene scene){
		this.name = name;
		
		entities =  scene.entities; 
		terrains = scene.terrains;
		
		textureLocations = scene.textureLocations;
		textures  = scene.textures;
		vertecies = scene.vertecies;
		texCoords = scene.texCoords;
		normals = scene.normals;
		indicies = scene.indicies;
		rawModels  = scene.rawModels;
		lights = scene.lights;
		shineDumpers = scene.shineDumpers;
		reflectivities = scene.reflectivities;
		hasTransparencies = scene.hasTransparencies;
		useFakeLightnings = scene.useFakeLightnings;
		mTexs = scene.mTexs;
	}

	public void Init()
	{
		
		if(mTexs.isEmpty()){
		
			if(textures.isEmpty()){
				for(String texL:textureLocations)
				 	textures.add(new Texture(texL));
			
			}else if(textureLocations.size() > textures.size()){
				int a = textureLocations.size() - textures.size() + 1;
				for(int i = 0; i < a; i++)
				textures.add(new Texture(textureLocations.get(textures.size() - 1 + i)));
			}
		
			for(Texture t:textures)
				mTexs.add(new ModelTexture(t));
			
		}else if(textures.size() > mTexs.size() ){
			int a = textures.size() - mTexs.size() + 1;
			for(int i = 0; i < a; i++)
				mTexs.add(new ModelTexture(textures.get(mTexs.size() - 1 + i)));
		}
		
		for(int i = 0; i < mTexs.size(); i++){
			mTexs.get(i).getTex().loadTexture();
			mTexs.get(i).setTextureID(mTexs.get(i).getTex().getId());
			mTexs.get(i).setReflectivity(reflectivities.get(i));
			mTexs.get(i).setShineDumper(shineDumpers.get(i));
			mTexs.get(i).setHasTranprency(hasTransparencies.get(i));
			mTexs.get(i).setUseFakeLighning(useFakeLightnings.get(i));
		}
		
		texAdded = false;

		for(int i = 0; i < indicies.size(); i++)
			rawModels.add(Loader.loadToVAO(vertecies.get(i), texCoords.get(i), normals.get(i), indicies.get(i)));
		rawModelAdded = false;
		
		
		if(!entities.isEmpty()){
			for(Entity e:entities){
				for(int r:e.getTextureRID())
					e.setTexture(mTexs.get(r));
				for(int r:e.getModelRID())
					e.setRawModel(rawModels.get(r));
			}
		}
		
		entitiyAdded = false;
		
		if(!terrains.isEmpty()){
			for(Terrain ter:terrains){
				ter.setTex(mTexs.get(ter.getTextureRID()));
				ter.setModel(ter.generateTerrain());
			}
		}
		
		terrainAdded = false;
		
		
		
		System.out.println(name + ":" + entities.size() + " " + indicies.size() + " " + rawModels.size());
		
		
	}

	
	public void Update()
	{
		for(Entity en:entities)
				en.Update();
		
	}
	
	
	public void Render(MasterRenderer renderer){
		
		if(texAdded){ 
			textures.add(new Texture(textureLocations.get(textureLocations.size() - 1)));
			textures.get(textures.size() - 1).loadTexture();
			mTexs.add(new ModelTexture(textures.get(textures.size() - 1)));
			mTexs.get(mTexs.size() - 1).setReflectivity(reflectivities.get(mTexs.size() - 1));
			mTexs.get(mTexs.size() - 1).setShineDumper(shineDumpers.get(mTexs.size() - 1));
			mTexs.get(mTexs.size() - 1).setHasTranprency(hasTransparencies.get(mTexs.size() - 1));
			mTexs.get(mTexs.size() - 1).setUseFakeLighning(useFakeLightnings.get(mTexs.size() - 1));
			texAdded = false;
			System.out.println(name + "A"+ ":" + entities.size() + " " + indicies.size() + " " + textures.size());
		}
		
		
		if(rawModelAdded){
			rawModels.add(Loader.loadToVAO(vertecies.get(indicies.size() - 1), texCoords.get(indicies.size() - 1), normals.get(indicies.size() - 1), indicies.get(indicies.size() - 1)));
			
			rawModelAdded = false;
			System.out.println(name + ":" + entities.size() + " " + indicies.size() + " " + mTexs.size());
		}
		
		if(entitiyAdded){
			
			for(int r:entities.get(entities.size() - 1).getTextureRID())
				entities.get(entities.size() - 1).setTexture(mTexs.get(r));
			for(int r:entities.get(entities.size() - 1).getModelRID())
				entities.get(entities.size() - 1).setRawModel(rawModels.get(r));
			
			entitiyAdded = false;
			System.out.println(name + ":" + entities.size() + " " + indicies.size() + " " + mTexs.size());
		}
		
		if(terrainAdded){
			terrains.get(terrains.size() - 1).setTex(mTexs.get(terrains.get(terrains.size() - 1).getTextureRID()));
			terrains.get(terrains.size() - 1).setModel(terrains.get(terrains.size() - 1).generateTerrain());
		}
		
		
		for(Entity en:entities)
			en.Render(renderer);
		
		for(Terrain ter:terrains)
			ter.Render(renderer);
	}
	
	
	private transient boolean entitiyAdded = false;
	public void addEntity(Entity en){
		entities.add(en);
		entitiyAdded = true;
	}
	
	
	private transient boolean terrainAdded = false;
	public void addTerrain(Terrain ter){
		terrains.add(ter);
		terrainAdded = true;
	}
	
	
	public void removeEntity(Entity en){
		for(int i = 0; i < entities.size(); i++){
				if(entities.get(i) == en){
					entities.remove(i);
				break;
			}
		}
	}
	
	public void removeEntityWithEverything(Entity en){
		for(int i = 0; i < entities.size(); i++)
			if(entities.get(i) == en)
				entities.remove(i);
		
		for(int j = 0; j < en.getModelRID().size(); j++){
			for(Entity e:entities)
				for(int k = 0; k < e.getModelRID().size(); k++){
					if(j != en.getModelRID().size() - 1)
						if(e.getModelRID().get(k) > en.getModelRID().get(j) && e.getModelRID().get(k) <= en.getModelRID().get(j))
							e.getModelRID().set(k, e.getModelRID().get(k) - (j + 1));
				}
		}
		
		
		for(int j = 0; j < en.getTextureRID().size(); j++){
			for(Entity e:entities)
				for(int k = 0; k < e.getTextureRID().size(); k++){
					if(j != en.getTextureRID().size() - 1)
						if(e.getTextureRID().get(k) > en.getTextureRID().get(j) && e.getTextureRID().get(k) <= en.getTextureRID().get(j))
							e.getTextureRID().set(k, e.getTextureRID().get(k) - (j + 1));
				}
		}
		
		
		
		for(int i:en.getModelRID()){
			rawModels.remove(i);
			indicies.remove(i);
			vertecies.remove(i);
			texCoords.remove(i);
			normals.remove(i);
		}
		
		for(int i:en.getTextureRID()){
			mTexs.remove(i);
			reflectivities.remove(i);
			shineDumpers.remove(i);
			textures.remove(i);
			textureLocations.remove(i);
		}
		
	}
	

	public List<Entity> getEntities() {
		return entities;
	}


	public String getName() {
		return name;
	}


	public Camera getCamera() {
		return camera;
	}




	public void setCamera(Camera camera) {
		this.camera = camera;
	}
	
	private transient boolean texAdded = false;
	public void addTexture(String textureLocation)
	{
		addTexture(textureLocation, 0, 0, false, false);
	}
	
	public void addTexture(String textureLocation, float reflectivity, float shineDumper, boolean hasTrasnparency,
			boolean useFakeLighning)
	{
		textureLocations.add(textureLocation);
		reflectivities.add(reflectivity);
		shineDumpers.add(shineDumper);
		hasTransparencies.add(hasTrasnparency);
		useFakeLightnings.add(useFakeLighning);
		texAdded = true;
	}
	
	private transient boolean rawModelAdded = false;
	public void addModel(float[] vertecies, float[] texCoords, float[] normals, int[] indicies){
		this.indicies.add(indicies);
		this.texCoords.add(texCoords);
		this.vertecies.add(vertecies);
		this.normals.add(normals);
		rawModelAdded = true;
		
	}
	
	public void addModel(String modelLocation){
		ModelData data = OBJLoader.loadOBJ(modelLocation);
		addModel(data.getVertices(), data.getTextureCoords(), data.getNormals(), data.getIndices());
	}
	
	
	
	public static void sendEntity(Entity en, Scene sender, Scene reciever){
		
		
		for(int i:en.getModelRID())
			reciever.addModel(sender.vertecies.get(i), sender.texCoords.get(i), sender.normals.get(i), sender.indicies.get(i));
		
		for(int i:en.getTextureRID())		
			reciever.addTexture(sender.textureLocations.get(i));
	
		for(int i = 0; i < en.getModelRID().size(); i++)
			en.getModelRID().set(i, reciever.indicies.size() - (i + 1));
			
		for(int i = 0; i < en.getTextureRID().size(); i++)
			en.getTextureRID().set(i, reciever.textureLocations.size() - (i + 1));
	
		System.out.println(reciever.indicies.size() - 1);
		reciever.addEntity(en);
		sender.removeEntity(en);

	}
	
	
	public static void sendEntityAndRemoveModels(Entity en, Scene sender, Scene reciever){
		
		
		for(int i:en.getModelRID())
			reciever.addModel(sender.vertecies.get(i), sender.texCoords.get(i), sender.normals.get(i), sender.indicies.get(i));
		
		for(int i:en.getTextureRID())		
			reciever.addTexture(sender.textureLocations.get(i));
	
		for(int i = 0; i < en.getModelRID().size(); i++)
			en.getModelRID().set(i, reciever.indicies.size() - (i + 1));
			
		for(int i = 0; i < en.getTextureRID().size(); i++)
			en.getTextureRID().set(i, reciever.textureLocations.size() - (i + 1));
	
		System.out.println(reciever.indicies.size() - 1);
		reciever.addEntity(en);
		
		sender.removeEntityWithEverything(en);
		
		
	}



	public List<Light> getLights() {
		return lights;
	}



	public void addLight(Light light) {
		lights.add(light);
	}
	
	
	
	
	
	
	

}
