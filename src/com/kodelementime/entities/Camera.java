package com.kodelementime.entities;

import org.joml.Vector3f;
import org.lwjgl.glfw.GLFW;

import com.kodelementime.core.Core;

public class Camera {

	private Vector3f position;
	private float pitch;
	private float yaw;
	private float roll = 0;
	
	
	private float closeScale = 0.5f;

	
	private float speed = 0.1f; 
	private boolean isOrtographic = false;
	
	public Camera(Vector3f pos) {
		position = pos;
	}
	
	public Camera(Vector3f pos, boolean isOrtographic) {
		position = pos;
		this.isOrtographic = isOrtographic;
		
	}
	
	
	public Camera(Camera cam)
	{
		position = cam.position;
		pitch = cam.pitch;
		yaw = cam.yaw;
		roll = cam.roll;
		isOrtographic = cam.isOrtographic;
	}
	
	public boolean isOrtographic() {
		return isOrtographic;
	}

	public void setOrtographic(boolean isOrtographic) {
		this.isOrtographic = isOrtographic;
	}

	public float getSpeed() {
		return speed;
	}
	
	public void setCloseScale(float closeScale) {
		this.closeScale = closeScale;
	}

	public float getCloseScale() {
		return closeScale;
	}

	public void setSpeed(float speed) {
		this.speed = speed;
	}



	public void Move(){
		
			if(Core.getKey(GLFW.GLFW_KEY_W)){
				if(isOrtographic())
					position.y -= speed * Math.sin(Math.toRadians(30));
				else
					position.y += speed * Math.sin(Math.toRadians(30));
				
				position.z -= speed * Math.cos(Math.toRadians(30));
		
			}
			if(Core.getKey(GLFW.GLFW_KEY_S)){
				if(isOrtographic())
					position.y += speed * Math.sin(Math.toRadians(30));
				else
					position.y -= speed * Math.sin(Math.toRadians(30));
				position.z += speed * Math.cos(Math.toRadians(30));
			}
			if(Core.getKey(GLFW.GLFW_KEY_A)){
				position.x -= speed;
			}
			if(Core.getKey(GLFW.GLFW_KEY_D)){
				position.x += speed;
			}
		
	}
	

	public Vector3f getPosition() {
		return position;
	}

	public void setPosition(Vector3f position) {
		this.position = position;
	}

	public float getPitch() {
		return pitch;
	}

	public void setPitch(float pitch) {
		this.pitch = pitch;
	}

	public float getYaw() {
		return yaw;
	}

	public void setYaw(float yaw) {
		this.yaw = yaw;
	}

	public float getRoll() {
		return roll;
	}

	public void setRoll(float roll) {
		this.roll = roll;
	}
	
	
	

}
