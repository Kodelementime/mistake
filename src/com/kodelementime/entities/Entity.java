package com.kodelementime.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.joml.Vector3f;


import com.kodelementime.models.RawModel;
import com.kodelementime.render.MasterRenderer;

import com.kodelementime.render.TMInstance;
import com.kodelementime.textures.ModelTexture;

public class Entity implements Serializable {
	
	
	private Vector3f position;
	private float rotX, rotY, rotZ;
	private float scale;
	
	private transient TMInstance instance;
	
	private List<Integer> textureRID = new ArrayList<Integer>();
	private List<Integer> modelRID = new ArrayList<Integer>();
	
	
	
	
	public Entity(int textureRID, int modelRID, Vector3f position, float rotX, float rotY, float rotZ, float scale) {
		super();
		this.textureRID.add(textureRID);
		this.modelRID.add(modelRID);
		this.position = position;
		this.rotX = rotX;
		this.rotY = rotY;
		this.rotZ = rotZ;
		this.scale = scale;
		this.instance = new TMInstance();
	}
	
	
	public Entity(List<Integer> textureRID, List<Integer> modelRID, Vector3f position, float rotX, float rotY, float rotZ, float scale) {
		super();
		this.textureRID = textureRID;
		this.modelRID = modelRID;
		this.position = position;
		this.rotX = rotX;
		this.rotY = rotY;
		this.rotZ = rotZ;
		this.scale = scale;
		this.instance = new TMInstance();
	}
	
	public Entity(List<Integer> textureRID, int modelRID, Vector3f position, float rotX, float rotY, float rotZ, float scale) {
		super();
		this.textureRID = textureRID;
		this.modelRID.add(modelRID);
		this.position = position;
		this.rotX = rotX;
		this.rotY = rotY;
		this.rotZ = rotZ;
		this.scale = scale;
		this.instance = new TMInstance();
	}
	
	
	public Entity(int textureRID, List<Integer> modelRID, Vector3f position, float rotX, float rotY, float rotZ, float scale) {
		super();
		this.textureRID.add(textureRID);
		this.modelRID = modelRID;
		this.position = position;
		this.rotX = rotX;
		this.rotY = rotY;
		this.rotZ = rotZ;
		this.scale = scale;
		this.instance = new TMInstance();
	}



	public RawModel getRawModel() {
		return instance.getModel();
	}


	public void setRawModel(RawModel rawModel) {
		this.instance.setModel(rawModel);;
	}


	public ModelTexture getTexture() {
		return instance.getTexture();
	}


	public void setTexture(ModelTexture texture) {
		this.instance.setTexture(texture);;
	}



	public List<Integer> getTextureRID() {
		return textureRID;
	}


	public void setTextureRID(List<Integer> textureRID) {
		this.textureRID = textureRID;
	}
	

	public TMInstance getInstance() {
		return instance;
	}


	public void setInstance(TMInstance instance) {
		this.instance = instance;
	}


	public List<Integer> getModelRID() {
		return modelRID;
	}


	public void setModelRID(List<Integer> modelRID) {
		this.modelRID = modelRID;
	}


	public void Render(MasterRenderer renderer){
		renderer.processEntity(this);
	}

	
	public void Update() {
		//increaseRotation(0, 0.1f, 0);
		
	}
	
	
	/*public static void addEntityToList(Entity e){
		if(entities != null){
				Entity[] tmp;	
				tmp = entities;
				entities = new Entity[tmp.length + 1];
				for(int i = 0; i < tmp.length; i++)
					entities[i] = tmp[i];
				entities[tmp.length] = e;
				System.err.println(entities[tmp.length].getModel().getRawModel().getVertexCount());
			}else{
				entities = new Entity[1];
				entities[0] = e;
				System.err.println(entities[0].getModel().getRawModel().getVertexCount());
			}
		
	}*/
	
	public void increasePosition(float dx, float dy, float dz){
		this.position.x += dx;
		this.position.y += dy;
		this.position.z += dz;
	}
	
	public void increaseRotation(float dx, float dy, float dz){
		rotX += dx;
		rotY += dy;
		rotZ += dz;
	}
	

	public Vector3f getPosition() {
		return position;
	}
	public void setPosition(Vector3f position) {
		this.position = position;
	}
	public float getRotX() {
		return rotX;
	}
	public void setRotX(float rotX) {
		this.rotX = rotX;
	}
	public float getRotY() {
		return rotY;
	}
	public void setRotY(float rotY) {
		this.rotY = rotY;
	}
	public float getRotZ() {
		return rotZ;
	}
	public void setRotZ(float rotZ) {
		this.rotZ = rotZ;
	}
	public float getScale() {
		return scale;
	}
	public void setScale(float scale) {
		this.scale = scale;
	}


	
	
	

}
