package com.kodelementime.shaders;

import org.joml.Matrix4f;
import org.joml.Vector3f;

import com.kodelementime.entities.Camera;
import com.kodelementime.entities.Light;
import com.kodelementime.tools.MathStuff;

public class TerrainShader extends ShaderProgram {

private static final String file = "src/com/kodelementime/shaders/terrainShader";
	
	private int location_transformationMatrix;
	private int location_projectionMatrix;
	private int location_viewMatrix;
	private int location_isOG;
	private int location_orthoProjectionMatrix;
	private int location_lightPosition;
	private int location_lightColor;
	private int location_shineDumper;
	private int location_reflectivity;
	private int location_skyColor;
	
	public TerrainShader() {
		super(file);
	}

	@Override
	protected void bindAttributes() {
		super.bindAttribute(0, "position");
		super.bindAttribute(1, "texCoords");
		super.bindAttribute(2, "normal");
	}

	@Override
	protected void getAllUniformLocations() {
		location_transformationMatrix = getUniformLocation("transformationMatrix");
		location_projectionMatrix = getUniformLocation("projectionMatrix");
		location_viewMatrix = getUniformLocation("viewMatrix");
		location_orthoProjectionMatrix = getUniformLocation("orthoProjectionMatrix");
		location_isOG = getUniformLocation("isOrthographic");
		location_lightPosition = getUniformLocation("lightPosition");
		location_lightColor = getUniformLocation("lightColor");
		location_shineDumper = getUniformLocation("shineDumper");
		location_reflectivity = getUniformLocation("reflectivity");
		location_skyColor = getUniformLocation("skyColor");
	}
	
	public void loadShineValues(float dumper, float reflectivity){
		super.loadFloat(location_shineDumper,dumper);
		super.loadFloat(location_reflectivity,reflectivity);
	}
	
	
	public void loadSkyColor(Vector3f color){
		super.loadVector(location_skyColor, color);
	}
	
	
	public void loadLight(Light light){
		super.loadVector(location_lightPosition, light.getPosition());
		super.loadVector(location_lightColor, light.getColor());
	}
	
	
	public void loadTransformationMatrix(Matrix4f matrix){
		super.loadMatrix(location_transformationMatrix, matrix);
	}
	
	public void loadViewMatrix(Camera c){
		Matrix4f matrix = MathStuff.createViewMatrix(c);
		super.loadMatrix(location_viewMatrix, matrix);
		super.loadBoolean(location_isOG, c.isOrtographic());
	}
	
	public void loadProjectionMatrix(Matrix4f matrix){
		super.loadMatrix(location_projectionMatrix, matrix);
	}

	public void loadOrthoProjectionMatrix(Matrix4f matrix) {
		super.loadMatrix(location_orthoProjectionMatrix, matrix);
		
	}

}
