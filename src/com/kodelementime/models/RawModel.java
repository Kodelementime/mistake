package com.kodelementime.models;

public class RawModel{
	
	private int VaoID;
	private int vertexCount;
	public RawModel(int VaoID, int vertexCount)
	{
		this.VaoID = VaoID;
		this.vertexCount = vertexCount;
	}
	
	public int getVaoID() {
		return VaoID;
	}
	public void setVaoID(int vaoID) {
		VaoID = vaoID;
	}
	public int getVertexCount() {
		return vertexCount;
	}
	public void setVertexCount(int vertexCount) {
		this.vertexCount = vertexCount;
	}
	
	
}