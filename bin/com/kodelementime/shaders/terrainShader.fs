#version 400 core

in vec2 pass_texCoords;
in vec3 surfaceNormal;
in vec3 toLightVector;
in vec3 toCameraVector;
in float visibility;
in float orthoGraph;

out vec4 out_Color;


uniform sampler2D textureSampler;

uniform vec3 lightColor;
uniform vec3 skyColor;

uniform float shineDumper;
uniform float reflectivity;

void main(void){

	vec3 uNormal = normalize(surfaceNormal);
	vec3 uLightVector = normalize(toLightVector);
	vec3 uCameraVector = normalize(toCameraVector);
	vec3 reflectedLightDirection = reflect(-uLightVector, uNormal);
	
	float specularFactor = dot(reflectedLightDirection, uCameraVector);
	specularFactor = max(specularFactor, 0.0);
	float dumpedFactor = pow(specularFactor, shineDumper);
	vec3 finalSpecular = dumpedFactor * lightColor;
	
	float nDot1 = dot(uNormal, uLightVector);
	float brightness = max(nDot1, 0.2);
	vec3 diffuse = brightness * lightColor;
	
	
	
	out_Color = vec4(diffuse,1.0) * texture(textureSampler, pass_texCoords) + vec4(finalSpecular, 1.0);
	if(orthoGraph < 0.5) out_Color = mix(vec4(skyColor, 1.0), out_Color, visibility);
}